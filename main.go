package main

import (
	"basic-web/handler"
	"net/http"
)

func main() {
	http.HandleFunc("/", handler.Index)
	http.HandleFunc("/barang", handler.GetBarang)
	http.ListenAndServe(":5001", nil)
}
