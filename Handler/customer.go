package handler

import (
	"basic-web/models"
	"fmt"
	"html/template"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintln(w, "Welcome To My Page")
	// fmt.Fprintln(w, "Yoyoyoyowwkwo")

	t, err := template.ParseFiles("./Html/template_index.html")
	if err != nil {
		fmt.Println(err)
	}

	cus := models.Customer{"Joel", 21}

	// cus => parse data cus lalu eksekusi
	t.Execute(w, cus)
}
